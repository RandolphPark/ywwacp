// JavaScript Document
// Common javascript functions for admin
var BASE_URL = "http://ywwacp-dev.azurewebsites.net";
var w,h;

function get_time_zone_offset( ) {
   var current_date = new Date( );
   var gmt_offset = 0 - current_date.getTimezoneOffset( ) / 60;
   return gmt_offset;
};


function adminLogout(){
	
    sessionStorage.removeItem("ywwacp.adminauth");
	sessionStorage.removeItem("ywwacp.adminname");
	window.location.href="index.html";
	
}


function adminLogin (loginData){
	$.ajax({
    	method: 'POST',
    	url: BASE_URL+'/Token',
    	data: loginData,
		async:false
	  }).done(function (data) {
		console.log ('sucess' + data);
		alert(data.userName + "   Successfully log in");
		// Cache the access token in session storage.
		sessionStorage.setItem("ywwacp.adminauth", data.access_token);
		sessionStorage.setItem("ywwacp.adminname", data.userName);
		window.location.reload();
	  }).fail(function( jqXHR, textStatus, errorThrown) {
	  	alert( "Log In failed:\n " + errorThrown +":"+ jqXHR.responseText );
	  });
	

}

function registerInterest(auth, info, details, callback){
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}

	$.ajax({
		type: 'POST',
		url: BASE_URL+info,
		dataType: "json", 
	  	data : details,
	  	contentType: "application/json",
		headers: headers
	}).done(function (data) {
		alert( "Register success.");
		callback();
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
		alert( "Register failed:\n " + errorThrown +":"+ jqXHR.responseText );
	});
}

function getUserInfo (auth, type, callback){
	
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}

	$.ajax({
		type: 'GET',
		url: BASE_URL+ type,
		async:false,
		headers: headers
	}).done(function (data) {
		callback(data);
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
		alert("Log in failed. \n"+textStatus+": "+errorThrown);    
	});

}

function createUser(auth, info, userDetails, callback){
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}

	$.ajax({
		type: 'POST',
		url: BASE_URL+info,
		dataType: "json", 
	  	data : userDetails,
	  	contentType: "application/json",
		headers: headers
	}).done(function (data) {
		alert( "Create success.");
		callback();
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
		alert("Create a user failed. \n"+ errorThrown +":"+ jqXHR.responseText );   
	});
	  
}

function updateUser(auth, id, userinfo, callback){
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}
	$.ajax({
		type: 'PUT',
		url: BASE_URL+ id,
		headers: headers,
		data: userinfo,
		dataType: "json", 
		crossDomain:true,
		async:false,
		contentType: "application/json",
	}).done(function (data) {
		
		callback(data);
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
		alert("Account information update failed. \n"+ errorThrown +":"+ jqXHR.responseText );  
	
	});
	

}

function deleteUser (auth, id, callback){
	
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}
	$.ajax({
		type: 'DELETE',
		url: BASE_URL+ id,
		headers: headers,

	}).done(function (data) {
		alert( "Delete success.");
		callback();
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
		alert("Delete failed. \n"+ errorThrown +":"+ jqXHR.responseText );   
	});
	
}


function postContent(auth, info, contentdata, callback){
	
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}
	$.ajax({
		type: 'POST',
		url: BASE_URL+info,
		headers: headers,
		data: contentdata,
		contentType: "application/json"
	}).done(function (data) {
		callback(data);
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
			alert("Operation failed. \n"+ errorThrown +":"+ jqXHR.responseText );   
	});
	
  
}


function updateContent (auth, info, id, data, callback){


	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}
	$.ajax({
		type: 'PUT',
		url: BASE_URL+info+ "/"+ id,
		headers: headers,
		data: data,
		contentType: "application/json"
	}).done(function (data) {
		callback(data);
	}).fail(function(jqXHR, textStatus, errorThrown) {
			
		alert("Information update failed. \n"+ errorThrown +":"+ jqXHR.responseText );    
	});
	
}

function deleteContent (auth, info, id, callback){
	var token = sessionStorage.getItem("ywwacp.adminauth");
	var headers = {};
	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}
	var request = $.ajax({ 

	  url: BASE_URL+ info + "/"+ id,
	  type: "DELETE",
	  headers:headers
  
	 });
	  request.done(function( msg ) {
		callback();
	  });
	  request.fail(function( jqXHR, textStatus, errorThrown ) {
	  	alert( "Delete failed:\n " + errorThrown +":"+ jqXHR.responseText );
	  });
}

$(document).ready( function() {
	
	//put the ajax loading animation to all pages
	  $('body').append('<div id="ajaxBusy"><p><img src="../images/ajax_loading_flower.gif"></p></div>');
	  w = Math.round(window.innerWidth * 0.5);
	  h = Math.round(window.innerHeight *0.5-20);	  
	  $('#ajaxBusy').css({
		display:"none",
		margin:"0px",
		padding:"0px",
		position:"absolute",
		left:"0px",
		top: "0px",
		width:"auto"
	 });
	 
	$('footer').html("&copy; Younger Women's Wellness Program");

	var header_content =
        '<div class="container-fluid"><a class="brand" href="#">Administration</a>		\
          <div class="btn-group pull-right">			\
			<a class="btn" href="my-profile.html"><i class="icon-user"></i><span id="displayname"> Admin</span></a> \
            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">	\
              <span class="caret"></span>		\
          </a>	\
           	<ul class="dropdown-menu">							\
			  <li><a href="my-profile.html">Profile</a></li>	\
              <li class="divider"></li>							\
              <li><a href="Javascript:adminLogout()">Logout</a></li>					\
            </ul>												\
          </div>												\
          <div class="nav-collapse">							\
            <ul class="nav">									\
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Registered Users <b class="caret"></b></a>													\
				<ul class="dropdown-menu">										\
					<li><a href="new-participant.html">New Participant</a></li>		\
					<li class="divider"></li> 									\
					<li><a href="index.html">All Registrations</a></li> 				\
					</ul>														\
			  </li>																\
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Specialist & Admin <b class="caret"></b></a>															\
				<ul class="dropdown-menu">												\
					<li><a href="new-specialist.html">New Specialist/Admin</a></li>		\
					<li class="divider"></li>											\
					<li><a href="specialist.html">Manage Specialists</a></li>			\
				</ul>											\
			  </li>												\
            </ul>												\
          </div> </div>';
		
	$('div.navbar-inner').html(header_content);
	
	var sidebar_content =
		'<ul class="nav nav-list">														\
              <li class="nav-header"><i class="icon-wrench"></i> Administration</li>	\
              <li><a href="index.html">Registered Users</a></li>						\
			  <li><a href="participant.html">Participants</a></li>						\
			  <li><a href="specialist.html">Specialists</a></li>							\
			  <hr> \
			  <li class="nav-header"><i class="icon-user"></i> Profile</li>				\
              <li><a href="my-profile.html">My profile</a></li>							\
			  <li><a href="Javascript:adminLogout()">Logout</a></li> 					\
			  <hr> \
			  <li class="nav-header"><i class="icon-pencil"></i> Contentt</li>	\
              <li><a href="newsfeed.html">News</a></li>						\
			  <li><a href="forumfeed.html">Forum</a></li>						\
           </ul> \
		   <hr> \
		   <div style="padding:20px 10px"> \
          	<img src="../images/header_parti_logo.png"/> \
          </div>'
	$('div.well.sidebar-nav').html(sidebar_content);
	
	
	var adminLogin_content = 
		'<div class="row-fluid">									\
			<div class="page-header">								\
				<h1>Admin Login <small>login info</small></h1>		\
			</div>													\
			<form class="form-horizontal" id ="adminLogin" >							\
				<fieldset>											\
					<div class="control-group">						\
						<label class="control-label" for="email">Username</label>		\
						<div class="controls">						\
							<input type="text" class="input-xlarge" id="email" value="" />	\
						</div>										\
					</div>											\
					<div class="control-group">						\
						<label class="control-label" for="password">Password</label>	\
						<div class="controls">											\
							<input type="password" class="input-xlarge" id="passw" value="" />	\
						</div>										\
					</div>											\
					<div class="form-actions">						\
						<input id="adminLogin_btn" type="submit" class="btn btn-success btn-large" value="Log In" />	\
					</div>						\
				</fieldset>						\
			</form>								\
		  </div>';  		
	
	
	
	//var cookieUserValue = $.cookie("ywwacp.adminname");
	//var cookieAuth = $.cookie("wwacp.adminauth");
	var adminname = sessionStorage.getItem ("ywwacp.adminname");
	var token = sessionStorage.getItem("ywwacp.adminauth");
	if (token != undefined){
	//if(cookieUserValue && cookieAuth) {
		$('span#displayname').html(adminname);
		
	 } else {
		
		$("div.span9").html(adminLogin_content);
	 }
	 
	 var changePassword = 
		'<div class="row-fluid">									\
			<div class="page-header">								\
				<h1>Change Password <small>login info</small></h1>		\
			</div>													\
			<form class="form-horizontal" id="adminLogin">							\
				<fieldset>											\
					<div class="control-group">						\
						<label class="control-label" for="email">E-mail</label>		\
						<div class="controls">						\
							<input type="email" class="input-xlarge" id="email" value="" />	\
						</div>										\
					</div>											\
					<div class="control-group">						\
						<label class="control-label" for="password">Password</label>	\
						<div class="controls">											\
							<input type="password" class="input-xlarge" id="passw" value="" />	\
						</div>										\
					</div>											\
					<div class="form-actions">						\
						<input id="adminLogin_btn" type="submit" class="btn btn-success btn-large" value="Log In" />	\
					</div>						\
				</fieldset>						\
			</form>								\
		  </div>';  		
	
	$('input#adminLogin_btn').click(function(){
	  var usr = $('form#adminLogin input#email').val();
	  var pwd = $('form#adminLogin input[type="password"]').val();
	  
	  var loginData = {
    	grant_type: 'password',
    	username: usr,
    	password: pwd
	  };
	  
	  adminLogin (loginData);	

	});
});

$(document).ajaxStart(function () {
	var d= $('#ajaxBusy');
	d.css({top:d.position().top+h+'px', left:w+'px'});
	
 	 $("#ajaxBusy").show();
});

$(document).ajaxStop(function () {
  $("#ajaxBusy").hide();
});	


/************** Export a table to CSV ******************************/
	
  
  var tableToExcel = (function () {
	  var uri = 'data:application/vnd.ms-excel;base64,'
		, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
		, base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
		, format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
	  return function (table, name) {
		  if (!table.nodeType) table = document.getElementById(table)
		  var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
		  window.location.href = uri + base64(format(template, ctx))
	  }
  })();