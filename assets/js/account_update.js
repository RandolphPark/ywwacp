// JavaScript Document
$(document).ready(function(e){

	/*************** Custom Date Validation Setting *********************/

	window.ParsleyValidator
	  .addValidator('dob', function (value, requirement) {
		var regu = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\s-]\d{4}$/;
		if (regu.test(value)) {
			var dob = moment(value, "DD/MM/YYYY");
			return dob.isValid(); 
		} else {
			return false;	
		}
	  }, 32)
	 .addValidator('newpassword', function (value, requirement) {
		var regu = /(?=.*\d)(?=.*[a-zA-Z]).{6,}/;
		if (regu.test(value)) {
			return true;
		} else {
			return false;	
		}
	  }, 33)
	  .addMessage('en', 'dob','It is not a valid date')
	  .addMessage('en', 'length','Some digit is missing') // Message
	  .addMessage('en', 'newpassword','Password must contain at least 6 characters, including UPPER/lowercase and at least one number') // Message
	  .addMessage('en', 'pattern','This filed should NOT contain number nor special characters') // Message

	/*************** Contact Number Formatting *********************/

	/*var formattedPhoneNumber = new Formatter(document.getElementById('phoneNumber'), {
	  'pattern': '({{99}}) {{9999}}-{{9999}}',
	  'persistent': false
	});*/

	var formattedPostCode = new Formatter(document.getElementById('postCode'), {
	  'pattern': '{{9999}}',
	  'persistent': false
	});


	
	
	/*************** Check box clicked *********************/
	
	 $( ".password-form" ).hide();
	 $('#password_check_box').attr("checked", false);
        
	 $('#password_check_box').change(function() {
        if($(this).is(":checked")) {
           // var returnVal = confirm("Are you sure?");
          //  $(this).attr("checked", returnVal);
          $( ".password-form" ).toggle( "blind", 500 );
        } else {
        console.log("unchecked");
        $( ".password-form" ).toggle( "blind", 500 );
        
        }
    });

	
	
	/*************** Register BTN PRESSED *********************/
	var account_url = "https://ww-api-test.azurewebsites.net/api/account/p"; 
	var auth =  $.cookie("wwacp.auth");
	//load user information 
	$.ajax(		
	{ 	     
		url: account_url,
		type: "GET",
		dataType: "json", 
		/*data: userinfo,*/
		beforeSend: function(jqXHR){
			jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
		},
		
		 success:function(data, textStatus, jqXHR) 
		{
			user_info = data;
			console.log(data);
			refreshFields (user_info);
			//data: return data from server; load the data to the field
			
		},
		
		error: function(jqXHR, textStatus, errorThrown) 
		{
			alert("Load Account failed !\n" + textStatus + ": " + jqXHR.responseText);
		}
	});
	
	$("#account_update_button").click(function() {
		// will check all the validaiton filed, and return true if ALL valid.
	  var isValid = $('.account-form').parsley().validate();
	  //console.log(isValid);
	  if(isValid){
		  updateUserAccount();
		   
		  if( ($('#newpassword').val().length>0) && ($('#newpassword').val() == $('#confirmpassword').val() ) ){
			  console.log("change password to "+$('#newpassword').val());
		  		changePassword($("#newpassword").val());
		  }
	  }
  
	});
	
	function get_time_zone_offset( ) {
	   var current_date = new Date( );
	   var gmt_offset = 0 - current_date.getTimezoneOffset( ) / 60;
	   return gmt_offset;
	};
	
	function refreshFields( user_info){
		
		$('#myemail').html ('  	'+user_info.Email);
		$('#dob').val(moment(new Date(user_info.DOB)).format ("DD/MM/YYYY"));		
		$('input[name="firstname"]').val(user_info.FirstName);
		$('input[name="lastname"]').val(user_info.LastName);
		$('#skypename').val( user_info.SkypeUsername );
		$('#facetimename').val(user_info.FacetimeUsername);
		$('#phoneNumber').val( user_info.Phone);
	
		$('#address').text(user_info.Address);
		$('#suburb').val( user_info.Suburb);
		$('#state option:selected').text( user_info.State);
		$('#postCode').val(user_info.PostCode);
		$('#country').val(user_info.Country);
		  
		  
	}
	
	function updateUserAccount(){
		var ddd = moment($('#dob').val(), "DD/MM/YYYY" ).format("YYYY-MM-DD");
		var userinfo ={};
		userinfo ={
		  
		  "Email": user_info.Email,
		  "DOB": ddd,
		  "Role":user_info.Role ,
		  "FirstName": $('input[name="firstname"]').val(),
		  "LastName": $('input[name="lastname"]').val(),	  
		  "SkypeUsername":$('input#skypename').val(),
		  "FacetimeUsername":$('input#facetimename').val(),
		  "Address": $('textarea#address').val(),
		  "Suburb": $('input#suburb').val(),
		  "State": $('select#state option:selected').text(),
		  "PostCode": $('input#postCode').val(),
		  "Country": $('input#country').val(),
		  "TimeOffset": get_time_zone_offset(),
		  "Phone": $('input#phoneNumber').val()
		  
		};
		
		userinfo = JSON.stringify(userinfo);
		
		$.ajax(
	  	{ 	     
			url: account_url,
			type: "PUT",
			dataType: "json", 
			data: userinfo,
			contentType: "application/json",
			beforeSend: function(jqXHR){
				jqXHR.setRequestHeader('Authorization', "Basic " + auth);
			},
			
			 success:function(data, textStatus, jqXHR) 
			{
				//data: return data from server
				alert("Account has been successfully updated!");
				location.reload();
			},
			
			error: function(jqXHR, textStatus, errorThrown) 
			{
				alert("Account information update failed !\n" + textStatus + ": " + jqXHR.responseText);
			}
		});
			
	}
	
	function changePassword( newpassword){
		var changepw_url = account_url+'/changepass';
		var encry_pw = btoa(newpassword);
		encry_pw = JSON.stringify(encry_pw);
		
		$.ajax(
	  	{ 	     
			url: changepw_url,
			type: "POST",
			dataType: "json", 
			contentType: "application/json; charset=utf-8",
			data: encry_pw,
			beforeSend: function(jqXHR){
				jqXHR.setRequestHeader('Authorization', "Basic " + auth);  
			},
			 success:function(data, textStatus, jqXHR) 
			{
				//update the cookie
				//data: return data from server
				alert("Password successfully changed !");
				$.removeCookie("auth");
				$.cookie("auth") = btoa($.cookie("username")+":"+newpassword);
				window.location.href = "home.html";
				
			},
			
			error: function(jqXHR, textStatus, errorThrown) 
			{
				alert("Failed to change password!\n" + textStatus + ": " + jqXHR.responseText);
			}
		});
		
	}

	//e.preventDefault(); //STOP default action
});

function checkPass()
	{
		//Store the password field objects into variables ...
		var pass1 = document.getElementById('newpassword');
		var pass2 = document.getElementById('confirmpassword');
		//Store the Confimation Message Object ...
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#66cc66";
		var badColor = "#ff6666";
		//Compare the values in the password field 
		//and the confirmation field
		if(pass1.value == pass2.value){
			//The passwords match. 
			//Set the color to the good color and inform
			//the user that they have entered the correct password 
			pass2.style.backgroundColor = goodColor;
			message.style.color = goodColor;
			message.innerHTML = "Passwords Match!"
		}else{
			//The passwords do not match.
			//Set the color to the bad color and
			//notify the user.
			pass2.style.backgroundColor = badColor;
			message.style.color = badColor;
			message.innerHTML = "Passwords Do Not Match!"
		}
	} 


