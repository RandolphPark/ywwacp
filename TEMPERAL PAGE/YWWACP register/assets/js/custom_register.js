// JavaScript Document
$(document).ready(function(){

	/*************** Custom Date Validation Setting *********************/

	window.ParsleyValidator
	  .addValidator('dob', function (value, requirement) {
		var regu = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\s-]\d{4}$/;
		if (regu.test(value)) {
			var dob = moment(value, "DD/MM/YYYY");
			return dob.isValid(); 
		} else {
			return false;	
		}
	  }, 32)
	  .addMessage('en', 'dob','It is not a valid date')
	  .addMessage('en', 'length','Some digit is missing') // Message
	  .addMessage('en', 'pattern','This filed should NOT contain number nor special characters') // Message

	/*************** Contact Number Formatting *********************/

	/*var formattedPhoneNumber = new Formatter(document.getElementById('phoneNumber'), {
	  'pattern': '({{99}}) {{9999}}-{{9999}}',
	  'persistent': false
	});*/

	var formattedPostCode = new Formatter(document.getElementById('postCode'), {
	  'pattern': '{{9999}}',
	  'persistent': false
	});


	  
	/*************** Register BTN PRESSED *********************/

	var register_url = YWWACP_BASE_URL + "/api/participation/lodge";
	$( ".register_button").click(function() {
		// will check all the validaiton filed, and return true if ALL valid.
	  var isValid = $('.registration-form').parsley().validate();
	  console.log(isValid);
	  if(isValid){
		  registerUser();
	  }
  
	});
	
	function get_time_zone_offset( ) {
	   var current_date = new Date( );
	   var gmt_offset = 0 - current_date.getTimezoneOffset( ) / 60;
	   return gmt_offset;
	};
	
	function registerUser(){
		var ddd = moment($('#dob').val(), "DD/MM/YYYY" ).format("YYYY-MM-DD");
		var userinfo ={};



		//   "DOB": "2015-05-20T06:45:36.0294126+00:00",
  // "Address": "sample string 4",
  // "Suburb": "sample string 5",
  // "State": "sample string 6",
  // "PostCode": "sample string 7",
  // "Country": "sample string 8",
  // "PhoneNumber": "sample string 9",
  // "Email": "sample string 10",
  // "FirstName": "sample string 11",
  // "LastName": "sample string 12",
  // "UserName": "sample string 13"

		userinfo ={
			"UserName": $('input[name="username"]').val(),
			"Email": $('input[type="email"]').val(),
			"FirstName": $('input[name="firstname"]').val(),
			"LastName": $('input[name="lastname"]').val(),
			"DOB": ddd,
			"Address": $('#address').val(),
			"Suburb": $('#suburb').val(),
			"State": $('#state option:selected').text(),
			"PostCode": $('#postCode').val(),
			"Country": $('#country').val(),
			"PhoneNumber": $('#phoneNumber').val(),
		};

		console.log(userinfo);
		console.log(JSON.stringify(userinfo));


		var headers = {};

		// $.ajax({
		// 	type: 'POST',
		// 	url: register_url,
		// 	dataType: "json", 
		//   	data : userinfo,
		//   	contentType: "application/json",
		//   	headers: headers
		// }).done(function (data) {
		// 	alert( "Register success.");
		// 	$(".registration-form").html("<h2 class='col-xs-offset-1 col-xs-10'>Thank you for registering your interest. We will contact you shortly</h2>");
		// }).fail(function(jqXHR, textStatus, errorThrown) {
				
		// 	alert( "Register failed:\n " + errorThrown +":"+ jqXHR.responseText );
		// });

		$.ajax(
	  	{ 	     
			url: register_url,
			crossDomain: true,
			type: "POST",
			dataType: "json", 
			data: JSON.stringify(userinfo),
			contentType: "application/json",
			headers: headers,
			 success:function(data, textStatus, jqXHR) 
			{
				//data: return data from server
				console.log("Successful");
				$(".registration-form").html("<h2 class='col-xs-offset-1 col-xs-10'>Thank you for registering your interest. We will contact you shortly</h2>");
				
			},
			
			error: function(jqXHR, textStatus, errorThrown) 
			{
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);

				alert("Registration failed !\n" + textStatus + ": " + jqXHR.responseText);
			}
		});
			
	}

	//e.preventDefault(); //STOP default action
});