// URL 
YWWACP_BASE_URL = "https://ywwacp-dev.azurewebsites.net";

// pink color #F16379
// blue color #00245E


// This file should be just followed after the jquery library. 
$( document ).ready( function(e) {
	
	 $("head").append('<link rel="shortcut icon" href="images/favicon.ico" /><script type="text/javascript" src="lib/jquery.cookie.js"></script>');
	// _____________ Header Section ______________________ //
	var header_content = '<h1 class="col-md-4 col-lg-4 col-sm-4 col-md-offset-1 col-lg-offset-1 col-sm-offset-1 hidden-xs"><a href="index.html"><img id="header_logo_image" src="assets/img/header_parti_logo.png" alt="" /></a></h1>        \
						<div class="signin-portal col-sm-5 col-sm-offset-2 col-md-offset-3 col-lg-offset-2 col-md-4 col-lg-5 col-xs-12">   								  \
							<h3 class="hidden-xs"><a href="register.html" >REGISTER INTEREST</a> </h3> 		   								 \
						</div>';														
	var navigation_content ='<ul class="nav navbar-nav">										\
								  <li class="col-sm-4"><a href="index.html">Home</a></li>					\
								  <li class="col-sm-4"><a href="register.html">Register</a></li>			\
								  <li class="col-sm-4"><a href="contact us.html">Contact Us</a></li>				\
							</ul>';
						
	$("#header_content_js").html(header_content);
	$("#navigation_content_js").html(navigation_content);
	
	
	// _____________ Footer Section ______________________ //
	var footer_content = '<div class="page row">																		\
					<dl class="col-lg-3 col-md-2 col-md-offset-1 col-lg-offset-1 col-sm-5 col-sm-offset-1 col-xs-offset-1 col-xs-11">																	\
						<dt>About the Program</dt>																	\
						<dd>																	\
							<ul>																	\
								<li><a href="index.html#wwacp_description">The Younger Womens Wellness after Cancer Program</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl class="col-lg-3 col-lg-offset-0 col-md-offset-0 col-md-2 col-sm-5 col-sm-offset-1 col-xs-offset-1 col-xs-11">																	\
						<dt>Registration</dt>																	\
						<dd>																	\
							<ul>																	\
								<li><a href="register.html">Register Interest</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl class="col-lg-2 col-lg-offset-0 col-md-offset-0 col-md-2 col-sm-5 col-sm-offset-1 col-xs-offset-1 col-xs-11">																	\
						<dt>Contact Us</dt>																	\
						<dd>																	\
							<ul>																	\
                            	<li><a href="contact us.html#enquire_section">Enquire</a></li>																	\
								<li><a href="mailto:wwacp@qut.edu.au">wwacp@qut.edu.au</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl class="col-lg-3 col-lg-offset-0 col-md-offset-0  col-md-3 col-sm-5 col-sm-offset-1 col-xs-offset-1 col-xs-11">																	\
						<dt>Our Partners</dt>																	\
						<dd><!-- End Navigation content -->																	\
                              <div class="partner_slider">																	\
                                  <ul class="bxslider">																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS_RBWH.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-BCNA.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-CQU.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-Melb.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-Notredame.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-PAH.png" alt="" /></li>																	\																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-RPA.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-St.John.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-sydney.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-UCSF.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-UQ.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-wesley.png" alt="" /></li>																	\
                                  </ul>																	\
                              </div>																	\
                              <!-- End Slider content -->																	\
                        </dd>																	\
					</dl>																	\
																						\
					<div class="policy row">																	\
						<ul class="col-lg-offset-1 col-lg-3 col-sm-20 col-xs-20 col-md-5 col-md-offset-1 col-xs-offset-1 col-md-offset-1">																	\
							<li><a href="privacy.html">Privacy</a></li>																	\
							<li><a href="copyright.html">Copyright</a></li>																	\
							<li class="none"><a href="disclaimer.html">Disclaimer</a></li>																	\
						</ul>																	\
					</div>																	\
				</div> ';
	
	
	$("#append_footer_js").html(footer_content);

	portal_update();
	
});

function portal_update(){
		var userwithoutlogin = '<li><a href="login.html" style="font-size:1.6em">Sign in</a></li>  <li><a href="register.html" style="font-size:1.6em">Register</a></li> '; 
	  var cookieUserValue = $.cookie("wwacp.username");
	  
	  var loginuser = '<li> <a href="account.html"> Welcome   <b>'+ cookieUserValue +'</b></a></li>  <li><button id= "logout">   Sign Out   </button ></li> ';
	  
	  
	  if(cookieUserValue) {
		   $("#user").html(loginuser);
		  
	  } else {
		  
			$("#user").html(userwithoutlogin);
	  }
	   
	 $("#logout").click(function() {
		//console.log("test");
		$.removeCookie("wwacp.username");
		$.removeCookie("wwacp.auth");
		location.reload();				 
		 
	 });
	 
	 //put the ajax loading animation to all pages
	  $('body').append('<div id="ajaxBusy"><p><img src="images/ajax_loading_flower.gif"></p></div>');
	 
	  $('#ajaxBusy').css({
		display:"none",
		margin:"0px",
		padding:"0px",
		position:"absolute",
		right:"0px",
		top:"0px",
		width:"auto"
	 });
		
}

$(document).ajaxStart(function () {
	var w = Math.round(window.innerWidth * 0.5);
	var h = Math.round(window.innerHeight *0.5-20);	 
	var d= $('#ajaxBusy');
	d.css({top:d.position().top+h+'px', left:w+'px'});
  $("#ajaxBusy").show();
});

$(document).ajaxStop(function () {
  $("#ajaxBusy").hide();
});	
