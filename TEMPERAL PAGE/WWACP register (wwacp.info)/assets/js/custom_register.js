// JavaScript Document
$(document).ready(function(){

	/*************** Custom Date Validation Setting *********************/

	window.ParsleyValidator
	  .addValidator('dob', function (value, requirement) {
		var regu = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\s-]\d{4}$/;
		if (regu.test(value)) {
			var dob = moment(value, "DD/MM/YYYY");
			return dob.isValid(); 
		} else {
			return false;	
		}
	  }, 32)
	  .addMessage('en', 'dob','It is not a valid date')
	  .addMessage('en', 'length','Some digit is missing') // Message
	  .addMessage('en', 'pattern','This filed should NOT contain number nor special characters') // Message

	/*************** Contact Number Formatting *********************/

	/*var formattedPhoneNumber = new Formatter(document.getElementById('phoneNumber'), {
	  'pattern': '({{99}}) {{9999}}-{{9999}}',
	  'persistent': false
	});*/

	var formattedPostCode = new Formatter(document.getElementById('postCode'), {
	  'pattern': '{{9999}}',
	  'persistent': false
	});


	
		/****************Set up ajax indicator *******************/
	  $('body').append('<div id="ajaxBusy"><p><img src="assets/img/ajax_loading_flower.gif"></p></div>');
	  var winH = window.innerHeight/2;
	 
	  $('#ajaxBusy').css({
		display:"none",
		margin:"0px",
		padding:"0px",
		position:"absolute",
		right:"45%",
		top:winH,
		width:"auto"
	  });
	 
	  
	/*************** Register BTN PRESSED *********************/
	var register_url = "https://ww-api-test.azurewebsites.net/api/account/register"; 
	$( ".register_button").click(function() {
		// will check all the validaiton filed, and return true if ALL valid.
	  var isValid = $('.registration-form').parsley().validate();
	  console.log(isValid);
	  if(isValid){
		  registerUser();
	  }
  
	});
	
	function get_time_zone_offset( ) {
	   var current_date = new Date( );
	   var gmt_offset = 0 - current_date.getTimezoneOffset( ) / 60;
	   return gmt_offset;
	};
	
	function registerUser(){
		var ddd = moment($('#dob').val(), "DD/MM/YYYY" ).format("YYYY-MM-DD");
		var userinfo ={};
		userinfo ={
		
		  "Email": $('input[type="email"]').val(),
		  "FirstName": $('input[name="firstname"]').val(),
		  "LastName": $('input[name="lastname"]').val(),
		  "DOB": ddd,
		  "Address": $('#address').val(),
		  "Suburb": $('#suburb').val(),
		  "State": $('#state option:selected').text(),
		  "PostCode": $('#postCode').val(),
		  "Country": $('#country').val(),
		  "TimeOffset": get_time_zone_offset(),
		  "PhoneNumber": $('#phoneNumber').val(),
		};
		console.log(userinfo);
		
		$.ajax(
	  	{ 	     
			url: register_url,
			type: "POST",
			dataType: "json", 
			data: userinfo,
			beforeSend: function(jqXHR){
				$('#ajaxBusy').show(); 
			},
			
			 success:function(data, textStatus, jqXHR) 
			{
				$('#ajaxBusy').hide();
				//data: return data from server
				//console.log("Successful");
				$(".registration-form").html("<h2>Thank you for registering your interest. We will contact you shortly</h2>");
				
			},
			
			error: function(jqXHR, textStatus, errorThrown) 
			{
				$('#ajaxBusy').hide();
				alert("Registration failed !\n" + textStatus + ": " + jqXHR.responseText);
			}
		});
			
	}

	e.preventDefault(); //STOP default action
});