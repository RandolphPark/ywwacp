$(document).ready(function(){
	
/********************Slider*********************/	
  $('.bxslider').bxSlider({
	  auto: true,
	  mode: 'horizontal',
	  autoControls: true,
	  captions: true
	});
	
/********************** Tabbing Section ***************************/

$('.tab-content > div').hide();
	$('.tab-content > div:first').show();
	$('.tab-container ul li a').click(function() {
		$('.tab-container ul li').removeClass('active_tab');
		$(this).parent().addClass('active_tab');

		var blockList = $(this).attr('class');
		//alert(blockList)
		$('.tab-content >div').hide();
		$('.' + blockList).css({
			'display' : 'block'
		});
	});

});



