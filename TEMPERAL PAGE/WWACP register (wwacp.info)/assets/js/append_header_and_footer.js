// JavaScript Document

// This file should be just followed after the jquery library. 
$( document ).ready( function(e) {
	
	// _____________ Header Section ______________________ //
	var header_content = '<h1><a href="#"><img id="header_logo_image" src="assets/img/header_normal_logo.png" alt="" /></a></h1>        \
						<div class="signin-portal">   								  \
							<h3><a href="register.html">register interest</h3> 		 \
						</div>';
						
	$("#header_content_js").html(header_content);
	
	
	// _____________ Footer Section ______________________ //
	var footer_content = '<div class="page">																		\
					<dl>																	\
						<dt>About the Program</dt>																	\
						<dd>																	\
							<ul>																	\
								<li><a href="index.html#wwacp_description">The Womens Wellness after Cancer Study</a></li>																	\
																<li><a href="register.html">Register Interest</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl>																	\
						<dt>Contact Us</dt>																	\
						<dd>																	\
							<ul>																	\
                            	<li><a href="contact us.html#enquire_section">Enquire</a></li>																	\
								<li><a href="mailto:wwacp@qut.edu.au">wwacp@qut.edu.au</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl>																	\
						<dt>Our Partners</dt>																	\
						<dd><!-- End Navigation content -->																	\
                              <div class="partner_slider">																	\
                                  <ul class="bxslider">																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS_RBWH.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-BCNA.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-CQU.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-Melb.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-Notredame.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-PAH.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-RPA.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-St.John.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-sydney.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-UCSF.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-UQ.png" alt="" /></li>																	\
                                      <li><img src="images/partner_logo/WWACP-website PARTNERS-wesley.png" alt="" /></li>																	\
                                  </ul>																	\
                              </div>																	\
                              <!-- End Slider content -->																	\
                        </dd>																	\
					</dl>																	\
																						\
					<div class="policy">																	\
						<ul>																	\
							<li><a href="privacy.html">Privacy</a></li>																	\
							<li><a href="copyright.html">Copyright</a></li>																	\
							<li class="none"><a href="disclaimer.html">Disclaimer</a></li>																	\
						</ul>																	\
					</div>																	\
				</div> ';
	
	
	$("#append_footer_js").html(footer_content);

	
});

