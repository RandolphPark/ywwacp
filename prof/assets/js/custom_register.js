// JavaScript Document
$(document).ready(function(){

	/*************** Custom Date Validation Setting *********************/

	window.ParsleyValidator
	  .addValidator('dob', function (value, requirement) {
		var regu = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\s-]\d{4}$/;
		if (regu.test(value)) {
			var dob = moment(value, "DD/MM/YYYY");
			return dob.isValid(); 
		} else {
			return false;	
		}
	  }, 32)
	  .addMessage('en', 'dob','It is not a valid date')
	  .addMessage('en', 'length','Some digit is missing') // Message
	  .addMessage('en', 'pattern','This filed should NOT contain number nor special characters') // Message

	/*************** Contact Number Formatting *********************/

	/*var formattedPhoneNumber = new Formatter(document.getElementById('phoneNumber'), {
	  'pattern': '({{99}}) {{9999}}-{{9999}}',
	  'persistent': false
	});*/

	var formattedPostCode = new Formatter(document.getElementById('postCode'), {
	  'pattern': '{{9999}}',
	  'persistent': false
	});

	 
	  
	/*************** Register BTN PRESSED *********************/
	var register_url = "https://ww-api-test.azurewebsites.net/api/account/"; 
	//$( ".register_button").click(function() {
	$( "#registerNow").click(function() {
		// will check all the validaiton filed, and return true if ALL valid.
	  var isValid = $('.registration-form').parsley().validate();
	  console.log(isValid);
	  if(isValid){
		  registerUser(null,'register');
	  }
  
	});
	
	/*************** Quick Register *********************/
	var auth = $.cookie("wwacp.profauth");
	if (!auth){
		$('#quickRegist').attr('disabled','disabled');
	}
	
	$( "#quickRegist").click(function() {
		//check the three fields: first name, last name, email
		var firstname = $('input[name="firstname"]').val();
		var lastname = $('input[name="lastname"]').val();
		var email=$('input[type="email"]').val();
		if (firstname && lastname && email)
			registerUser (auth, 'registerparticipant');
		else
			alert ("Missing information!\nFirstname: "+firstname+'\nLastname: '+lastname +'\nEmail: '+email);

	});
	/********************************************/
	function get_time_zone_offset( ) {
	   var current_date = new Date( );
	   var gmt_offset = 0 - current_date.getTimezoneOffset( ) / 60;
	   return gmt_offset;
	};
	
	function registerUser( auth, type){
		var ddd = moment($('#dob').val(), "DD/MM/YYYY" ).format("YYYY-MM-DD");
		var userinfo ={};
		userinfo ={
		
		  "Email": $('input[type="email"]').val(),
		  "FirstName": $('input[name="firstname"]').val(),
		  "LastName": $('input[name="lastname"]').val(),
		  "DOB": ddd,
		  "Address": $('#address').val(),
		  "Suburb": $('#suburb').val(),
		  "State": $('#state option:selected').text(),
		  "PostCode": $('#postCode').val(),
		  "Country": $('#country').val(),
		  "TimeOffset": get_time_zone_offset(),
		  "PhoneNumber": $('#phoneNumber').val(),
		};
		//console.log(userinfo);
		
		$.ajax(
	  	{ 	     
			url: register_url + type,
			type: "POST",
			dataType: "json", 
			data: userinfo,
			beforeSend: function(jqXHR){
				
				if (auth != null && auth !== undefined ){
					jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
				}
			},
			
			 success:function(data, textStatus, jqXHR) 
			{
				
				alert ("Registration success !");
				window.location.reload();  
				//$(".registration-form").html("<h2>Thank you for registering your interest. We will contact you shortly</h2>");
				
			},
			
			error: function(jqXHR, textStatus, errorThrown) 
			{
				alert("Registration failed !\n" + textStatus + ": " + jqXHR.responseText);
			}
		});
			
	}


	
});