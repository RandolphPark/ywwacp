// JavaScript Document for Forum/Discussion Board
//var URL_PRE = "https://ww-api-test.azurewebsites.net/api/forum/";
//var auth = $.cookie("auth");

//formatting a time	
var formatTime = function(unixTimestamp) {
	var dt = new Date(unixTimestamp);
	//console.log('before:'+ dt);
	var hours = dt.getHours();
	var timeOffset = dt.getTimezoneOffset()/60;		
	//console.log (hours + '; '+timeOffset);
	dt.setHours(hours - timeOffset);
	//console.log('after:'+dt);

	var result = moment(dt).format("DD-MMM-YY HH:mm a");
	return result;
}

var page_num_of_threads = function(){
	$.ajax({ 
		url: URL_PRE+"threads/pages",
		type: "GET",
		dataType: "json", 
		contentType: "application/json",
		beforeSend: function(jqXHR){
		  jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
		},
		 success:function(number, textStatus, jqXHR) 
		 {
			// console.log ('number is '+number);
			 /*for (var i=0; i< number; i++){
				 var page = i+1;
				$(".pagination_section ul").append('<li><a href="Javascript: getThreads('+page+')">'+page+'</a></li>');
			 }
			 $(".pagination_section ul li:first-child").addClass('current');*/
			 threadsPagination( number);
			 return number;
		 },
		 error:function(jqXHR, textStatus, errorThrown) 	  
		  {
			  console.log("Failed to get the number of pages. \n"+textStatus+": "+errorThrown); 
			  return -1;			  
		  }
	});
	
}

var page_num_of_messages = function(id){
	
	$.ajax({ 
		url: URL_PRE+"thread/"+ id +"/pages",
		type: "GET",
		dataType: "json", 
		contentType: "application/json",
		beforeSend: function(jqXHR){
		  jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
		},
		 success:function(number, textStatus, jqXHR) 
		 {
			 /*for (var i=0; i< number; i++){
				 var page = i+1;
				$(".pagination_section ul").append('<li><a href="Javascript: getMessages('+id+','+page+')">'+page+'</a></li>');
			 }
			 $(".pagination_section ul li:first-child").addClass('current');*/
			 messagesPagination( number);
			 console.log (number);
			 return number;
			
		 },
		 error:function(jqXHR, textStatus, errorThrown) 	  
		  {
			  console.log("Failed to get the page number. \n"+textStatus+": "+errorThrown); 	
			  return -1;		  
		  }
	});
	
	
}


function getThreads (page_num){
	/*$('.pagination_section ul li').removeClass('current');
	$('.pagination_section ul li').eq(page_num-1).addClass('current');*/
	$.ajax({ 
    
		  url: URL_PRE+"threads?page="+page_num,
		  type: "GET",
		  dataType: "json", 
		  contentType: "application/json",
		  beforeSend: function(jqXHR){
			  jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
		  },
	  
		   success:function(data, textStatus, jqXHR) 
		  {
			  //data: return data from server
			  console.log(data);
			  $("div.thread_row" ).not(':first').remove(); 		//remove existing thread_section
			  //alert("Successfully get threads");
			  //load threads data to the "thread section" div
			  $.each (data, function (i, thread){
				 
				  var posts = thread.Messages;
				  var updatetime = formatTime(thread.LastActivityTimestamp);
				  //console.log (thread.Author.Role);

				  var imagename ;
				  if (thread.Author.Role == "specialist")
					  imagename = "../images/prof_icon.png";
				  else if (thread.Author.Role == "admin")
					  imagename = "../images/admin_icon.png";
				  else if (thread.Author.Role == "active") {
					  imagename = "../images/parti_active_icon.png";
				  }else
				  	  imagename = "../images/parti_inactive_icon.png";
				  
				  $(".thread_section").append(
					'<div class="thread_row">'+
					  '<div class="icon"><img src='+ imagename +' /></div>'+
					  '<div class="thread title"><a href="Javascript: openThread('+ thread.Id +');">'+thread.Title+'</a></div>'+
					  '<div class="posts">'+ posts +'</div>'+
					  '<div class="update">'+ updatetime +'</div>'+
					'</div>');
			  });
		  },
		  
		  error: function(jqXHR, textStatus, errorThrown) 	  
		  {
			  //if fails  
			  alert("Failed to load threads. "+textStatus+': '+errorThrown);   
			  console.log("Fail. \n"+textStatus+": "+errorThrown); 
			  
		  }
  
	 });
	
}

function postThread(data){
	$.ajax({ 
    
            url: URL_PRE+"thread",
            type: "POST",
            dataType: "json", 
            data : data,
            contentType: "application/json",
            beforeSend: function(jqXHR){
                jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
            },
      
            success:function(data, textStatus, jqXHR) 
            {
                //data: return data from server
                alert("Successfully Created");
                location.reload();
            },
            
            error: function(jqXHR, textStatus, errorThrown) 
    
            {
                //if fails  
                alert("Create a thread failed. \n"+textStatus+': '+errorThrown);
            }
  
        });
}


function getMessages( page){
	var id = thread_id;
	console.log("id is "+id+' '+page);
	$.ajax({
		url: URL_PRE+"thread/"+id+'/'+page,
		type: "GET",
		dataType: "json", 
		contentType: "application/json",
		beforeSend: function(jqXHR){
			jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
		},
	
		 success:function(data, textStatus, jqXHR) 
		{
			//data: return data from server
			console.log("Successful load messages");
			//console.log(data);
			$('.message').remove();
			$('.thread_topic').remove();
			showMessages(data);
		},
		
		error: function(jqXHR, textStatus, errorThrown) 
		{
			alert("Failed to load messages. \n"+textStatus+': '+errorThrown) ;  
			//if fails     
			console.log("Fail"); 
			console.log(textStatus);
			console.log(errorThrown);
		}

	});
	
};


function showMessages(data){
	var title = data.Title;
	
	$.each(data.Messages, function (i, info){
		var imagename ;
		if (info.Author.Role == "specialist")
			imagename = "../images/prof_icon.png";
		else if (info.Author.Role == "admin")
			imagename = "../images/admin_icon.png";
		else{
			imagename = "../images/parti_icon.png";
		} 		
		console.log (info);
	
		if (i==0){
			$('div.messages').append(
			'<div class="thread_topic">'+ title +'</div>' +
			'<div class="message">'	+ 	
				'<div id="content">'+ info.Message +'</div>' + 
				'<div id="time">'+ formatTime(info.Created) +'</div>' +
				'<div id="author">Posted by '+ info.Author.Name + '<img src='+ imagename +'/></div>' +
			'</div>');
		}else {
			$('div.messages').append(
			'<div class="message">'	+ 
				'<div id="content">'+ info.Message +'</div>' + 
				'<div id="time">'+ formatTime(info.Created) +'</div>' +
				'<div id="author">Posted by '+ info.Author.Name + '<img src='+ imagename +'/></div>' +
			'</div>');
		}	
			
	});			
	
}

function postMessage (id){
	var message = $('#message_text').val();
	message = JSON.stringify(message);
	console.log (message);

	$.ajax({
		url: URL_PRE+"thread/"+id,
		type: "POST",
		dataType: "json", 
		data: message,
		contentType: "application/json",
		beforeSend: function(jqXHR){
			jqXHR.setRequestHeader('Authorization', "Basic " + auth); 
		},
	
		 success:function(data, textStatus, jqXHR) 
		{
			//data: return data from server
			//console.log("Successful Created");
			alert ('Post answer successfully. '); 
			location.reload();
		},
		
		error: function(jqXHR, textStatus, errorThrown) 
		{
			//if fails    
			alert ('Post answer failed. \n'+ textStatus + ': '+errorThrown); 
	
		}

	});
}

function messagesPagination( total_pages){
	$(".pagination_section").pagination({
		pages: total_pages,
		currentPage: 1,
        cssStyle: 'light-theme',
		onPageClick: function(pageNumber) {
			getMessages (pageNumber); 
		}
	  });
	    
}

function threadsPagination( total_pages){
	$(".pagination_section").pagination({
		pages: total_pages,
		currentPage: 1,
        cssStyle: 'light-theme',
		onPageClick: function(pageNumber) {
			getThreads (pageNumber); 
		}
	  });
	    
}


// input : text_content -> string such as reply, message or comment
// return : true  -> contain inappropriate words  
// 			false -> not contain inappropriate words 
function hasSwears (text_content) {

	var bad_words_array=swearWords.words;
		var alert_arr=new Array;
		var alert_count=0;
		var compare_text=text_content;
		 
		for(var i=0; i<bad_words_array.length; i++){
			var pattern = new RegExp('\\b' + bad_words_array[i] + '\\b', 'g');
			if(compare_text.search(pattern) != -1) {
				alert_count++;
			}
		}
		
		if (alert_count===0) {
			return false;
		} else {
			return true;
		};

}