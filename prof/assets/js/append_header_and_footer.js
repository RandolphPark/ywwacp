// JavaScript Document

// This file should be just followed after the jquery library. 
$( document ).ready( function(e) {
	
	 $("head").append('<link rel="shortcut icon" href="../images/favicon.ico" />'); 
	// _____________ Header Section ______________________ //
	var header_content = '<h1><a href="#"><img id="header_logo_image" src="assets/img/header_prof_logo.png" alt="" /></a></h1>        \
						<div class="signin-portal">   								  \
							<h3>professional portal</h3>    						 \
							<ul id="user">    </ul>   									  \
						</div>';
						
	$("#header_content_js").html(header_content);
	var navigation_content ='<ul class="clearfix">										\
						  <li><a href="index.html">Home</a></li>					\
						  <li><a href="forum.html">Community</a></li>				\
						  <li><a href="#">Resources</a>								\
							<ul>														\
							 <li><a href="welcomeletter.html">Welcome Letter</a></li>	\
							  <li><a href="pod_cast.html">Podcasts</a></li>				\
							  <li><a href="news.html">News</a></li>						\
							  <li><a href="book.html">Books</a></li>					\
							   <li><a href="faqs.html">FAQ</a></li>						\
							</ul>														\
						  </li>															\
						  <li><a href="appointment.html">Consultation</a></li>			\
						  <li><a href="contact us.html">Contact Us</a></li>				\
						</ul> ';
	$("#navigation_content_js").html(navigation_content);
	
	// _____________ Footer Section ______________________ //
	var footer_content = '<div class="page">																		\
					<dl>																	\
						<dt>About the Program</dt>																	\
						<dd>																	\
							<ul>																	\
								<li><a href="#wwacp_description">The Womens Wellness After Cancer Program</a></li>																	\
								<li><a href="#wwacp_description">Why Participate</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl>																	\
						<dt>Registration</dt>																	\
						<dd>																	\
							<ul>																	\
								<li><a href="register.html">Register Interest</a></li>																	\
								<li><a href="../index.html">Participant Portal</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl>																	\
						<dt>Contact Us</dt>																	\
						<dd>																	\
							<ul>																	\
                            	<li><a href="contact us.html#enquire_section">Enquire</a></li>																	\
								<li><a href="mailto:wwacp@qut.edu.au">wwacp@qut.edu.au</a></li>																	\
							</ul>																	\
						</dd>																	\
					</dl>																	\
					<dl>																	\
						<dt>Our Partners</dt>																	\
						<dd><!-- End Navigation content -->																	\
                              <div class="partner_slider">																	\
                                  <ul class="bxslider">																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS_RBWH.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-BCNA.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-CQU.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-Melb.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-Notredame.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-PAH.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-RPA.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-St.John.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-sydney.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-UCSF.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-UQ.png" alt="" /></li>																	\
                                      <li><img src="../images/partner_logo/WWACP-website PARTNERS-wesley.png" alt="" /></li>																	\
                                  </ul>																	\
                              </div>																	\
                              <!-- End Slider content -->																	\
                        </dd>																	\
					</dl>																	\
																						\
					<div class="policy">																	\
						<ul>																	\
							<li><a href="privacy.html">Privacy</a></li>																	\
							<li><a href="copyright.html">Copyright</a></li>																	\
							<li class="none"><a href="disclaimer.html">Disclaimer</a></li>																	\
						</ul>																	\
					</div>																	\
				</div> ';
	
	
	$("#append_footer_js").html(footer_content);

	portal_update();
	
});

function portal_update(){
	var userwithoutlogin = '<li><a href="login.html">Sign in</a></li>  <li><a href="register.html">Register</a></li> '; 
	 // $("#user").html(userwithoutlogin);
	var cookieUserValue = $.cookie("wwacp.profusername");
	//console.debug(cookieUserValue);    
	//console.debug($.cookie("auth"));
	  
	var loginuser = '<li> <a href="account.html"> Welcome   <b>'+ cookieUserValue +'</b></a></li>  <li><button id= "logout">   Sign Out   </button ></li> ';
	    
	if(cookieUserValue) {
		    
	    $("#user").html(loginuser);
	    		    
	} else {
 
      $("#user").html(userwithoutlogin);
	    
	}
    				
	$("#logout").click(function() {
		 //alert("sign out");
		
		$.removeCookie("wwacp.profusername");
		$.removeCookie("wwacp.profauth");
		
		window.location.href = "index.html";
		//location.reload();				 
			 
	});
		
	
	/****************Set up ajax indicator *******************/
	  $('body').append('<div id="ajaxBusy"><p><img src="../images/ajax_loading_flower.gif"></p></div>');
	 
	  $('#ajaxBusy').css({
		display:"none",
		margin:"0px",
		padding:"0px",
		position:"absolute",
		right:"0px",
		top:"0px",
		width:"auto"
	  });
	  
}

	  
$(document).ajaxStart(function () {
	var w = Math.round(window.innerWidth * 0.5);
	var h = Math.round(window.innerHeight *0.5-20);	 
	var d= $('#ajaxBusy');
	d.css({top:d.position().top+h+'px', left:w+'px'});
	$("#ajaxBusy").show();
});

$(document).ajaxStop(function () {
	$("#ajaxBusy").hide();
});
